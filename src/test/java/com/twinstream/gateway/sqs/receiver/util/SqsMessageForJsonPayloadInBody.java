package com.twinstream.gateway.sqs.receiver.util;

import com.amazonaws.services.sqs.model.MessageAttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.UUID;

/**
 * Utility class to place messages onto a SQS Queue.
 * Set AWS credentials (-DAWS_ACCESS_KEY=your access key -DAWS_SECRET_KEY=your secret") on the commandline.
 */
public class SqsMessageForJsonPayloadInBody extends BaseSqsMessageProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final int MESSAGE_COUNT = 1;

    public static void main(String[] args) {
        new SqsMessageForJsonPayloadInBody()
                .withMessageCount(MESSAGE_COUNT)
                .generateAndSendMessageToQueue();
    }

    public SqsMessageForJsonPayloadInBody() {
        super();
    }

    @Override
    protected Map<String, MessageAttributeValue> getMessageAttributes(int i, UUID correlationId) {
        return new MessageAttributesBuilder()
                .withFlowMetadata()
                .withMessageMetadataForPayloadInBody(MessageAttributesBuilder.JSON_CONTENT_TYPE,
                                                     getPayload(i, correlationId).getBytes().length)
                .withCorrelationId(correlationId)
                .withAckMessage()
                .build();
    }

    @Override
    protected String getPayload(int i, UUID correlationId) {
        return new PayloadBuilder().withJsonPayload(i);
    }
}
