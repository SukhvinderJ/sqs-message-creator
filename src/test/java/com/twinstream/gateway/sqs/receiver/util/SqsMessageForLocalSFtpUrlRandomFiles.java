package com.twinstream.gateway.sqs.receiver.util;

import com.amazonaws.services.sqs.model.MessageAttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static com.twinstream.gateway.sqs.receiver.util.PayloadType.PAYLOAD_URL_BIG_01;
import static com.twinstream.gateway.sqs.receiver.util.PayloadType.PAYLOAD_URL_ONE;
import static com.twinstream.gateway.sqs.receiver.util.PayloadType.PAYLOAD_URL_VERY_BIG_FILE;

/**
 * Utility class to place messages onto a SQS Queue.
 * Set AWS credentials (-DAWS_ACCESS_KEY=your access key -DAWS_SECRET_KEY=your secret") on the commandline.
 */
public class SqsMessageForLocalSFtpUrlRandomFiles extends BaseSqsMessageProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final int MESSAGE_COUNT = 1000;

    public static void main(String[] args) {
        new SqsMessageForLocalSFtpUrlRandomFiles()
                .withMessageCount(MESSAGE_COUNT)
                .generateAndSendMessageToQueue();
    }

    public SqsMessageForLocalSFtpUrlRandomFiles() {
        super();
    }

    protected PayloadType randomPayloadType() {
        int distribution = ThreadLocalRandom.current().nextInt(1,100);
        if (distribution == 100) {
            return PAYLOAD_URL_VERY_BIG_FILE;
        } else if (distribution >= 98) {
            return PAYLOAD_URL_BIG_01;
        } else {
            return PAYLOAD_URL_ONE;
        }
    }

    @Override
    protected Map<String, MessageAttributeValue> getMessageAttributes(int i, UUID correlationId) {
        return new MessageAttributesBuilder()
                .withFlowMetadata()
                .withMessageMetadataForPayloadUrl(MessageAttributesBuilder.JSON_CONTENT_TYPE,
                                                  randomPayloadType())
                .withCorrelationId(correlationId)
                .withAckMessage()
                .build();
    }

    @Override
    protected String getPayload(int i, UUID correlationId) {
        return new PayloadBuilder().withEmptyPayload(i);
    }
}
