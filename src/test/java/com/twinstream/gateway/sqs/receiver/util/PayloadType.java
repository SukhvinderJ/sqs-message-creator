package com.twinstream.gateway.sqs.receiver.util;

public enum PayloadType {

    PAYLOAD_URL_ONE("sftp://sukh-user@localhost/upload/one.txt", 63L),
    PAYLOAD_URL_LARGE_SISL_FILE("sftp://sukh-user@localhost/upload/large-sisl-file", 2256L),
    PAYLOAD_URL_BIG_01("sftp://sukh-user@localhost/upload/big-01.txt", 677139368L),
    PAYLOAD_URL_VERY_BIG_FILE("sftp://sukh-user@localhost/upload/very-big.iso", 4781506560L),
    PAYLOAD_URL_SUKHYJ_ONE("sftp://xdgi_user1-dev-sukhyj@vpce-02c63b834f2e54f4d-4f1y41jy.server.transfer.eu-west-2.vpce.amazonaws.com/packaged-imports/one.txt", 63L),
    PAYLOAD_URL_SUKHYJ_MEDIUM("sftp://xdgi_user1-dev-sukhyj@vpce-02c63b834f2e54f4d-4f1y41jy.server.transfer.eu-west-2.vpce.amazonaws.com/packaged-imports/medium-01.txt", 58590955L);

    private final long payloadSize;

    private final String payloadUrl;

    PayloadType(String payloadUrl, long payloadSize) {
        this.payloadSize = payloadSize;
        this.payloadUrl = payloadUrl;
    }

    public long getPayloadSize() {
        return payloadSize;
    }

    public String getPayloadUrl() {
        return payloadUrl;
    }
}
