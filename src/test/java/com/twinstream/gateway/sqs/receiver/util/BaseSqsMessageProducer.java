package com.twinstream.gateway.sqs.receiver.util;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.UUID;

/**
 * Utility class to place messages onto a SQS Queue.
 * Set AWS credentials (-DAWS_ACCESS_KEY=your access key -DAWS_SECRET_KEY=your secret") on the commandline.
 */
abstract public class BaseSqsMessageProducer {

    protected static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

//    protected static final String SQS_URL = "https://sqs.eu-west-2.amazonaws.com/240338450779/gateway-sqs-receiver-dev";
    protected static final String SQS_URL = "https://sqs.eu-west-2.amazonaws.com/240338450779/xdg-packaging-complete-queue-dev-sukhyj";

    private int messageCount = 1;

    protected final AmazonSQS amazonSqs;

    protected BaseSqsMessageProducer() {
        BasicAWSCredentials credentials = new BasicAWSCredentials(System.getProperty("AWS_ACCESS_KEY"), System.getProperty("AWS_SECRET_KEY"));
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(credentials);

        this.amazonSqs = AmazonSQSClientBuilder.standard()
                                               .withCredentials(awsCredentialsProvider).build();
    }

    protected BaseSqsMessageProducer withMessageCount(int messageCount) {
        this.messageCount = messageCount;
        return this;
    }

    protected void generateAndSendMessageToQueue() {

        for (int i = 0; i < getMessageCount(); i++) {
            UUID correlationId = UUID.randomUUID();
            String payload = getPayload(i, correlationId);
            Map<String, MessageAttributeValue> messageAttributes = getMessageAttributes(i, correlationId);
            sendMessage(payload, messageAttributes);
            LOGGER.info("Sent message {}, correlationId: {}", i, correlationId);
        }
    }

    protected abstract Map<String, MessageAttributeValue> getMessageAttributes(int i, UUID correlationId);

    protected abstract String getPayload(int i, UUID correlationId);

    private void sendMessage(String payload, Map<String, MessageAttributeValue> messageAttributes) {
        SendMessageRequest sendMessageRequest = new SendMessageRequest(SQS_URL, payload);
        amazonSqs.sendMessage(sendMessageRequest.withMessageAttributes(messageAttributes));
    }

    protected int getMessageCount() {
        return messageCount;
    }
}
