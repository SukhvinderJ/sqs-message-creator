package com.twinstream.gateway.sqs.receiver.util;

import com.amazonaws.services.sqs.model.MessageAttributeValue;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MessageAttributesBuilder {

    private final Map<String, MessageAttributeValue> messageAttributes;

    static final String JSON_CONTENT_TYPE = "application/json";

    static final String SISL_CONTENT_TYPE = "application/sisl";

    private static final String DESCRIPTION = "Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit"
            + "%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.";

    private static final String FLOW_METADATA = "x-filename=the-x-filename.txt,"
            + "x-xd-flow-id=f4206bea-e871-49d9-9a41-d24746ee60ff,"
            + "x-xd-job-id=5713a3e6-73c8-4878-bfe9-97a3c606341b,"
            + "x-xd-classification=OFFICIAL,"
            + "x-xd-job-description=" + DESCRIPTION + ","
            + "x-xd-job-reference=the-x-xdo-import-reference,"
            + "x-xd-job-timestamp=2020-08-06T10:56:23.183Z";

    private static final String ackMessage = "{\"importId\":\"CBTSIV8YLDKVTL6R\",\"s3Path\":\"packaged-import/FILES"
            + "/CBTSIV8YLDKVTL6R.zip\",\"s3Bucket\":\"on-premise-transfer-test\",\"importType\":\"FILES\"}";

    private static final String MESSAGE_METADATA_EXCLUDING_PAYLOAD_URL = "content-type=%s,content-length=%d";

    private static final String MESSAGE_METADATA_INCLUDING_PAYLOAD_URL = "content-type=%s,content-length=%d,payload-url=%s";

    public MessageAttributesBuilder() {
        this.messageAttributes = new HashMap<>();
    }

    public MessageAttributesBuilder withFlowMetadata() {
        messageAttributes.put("flow-metadata", new MessageAttributeValue().withDataType("String").withStringValue(FLOW_METADATA));
        return this;
    }

    public MessageAttributesBuilder withMessageMetadataForPayloadUrl(String messageContentType, PayloadType payloadType) {
        messageAttributes.put("message-metadata", new MessageAttributeValue().withDataType("String")
                .withStringValue(
                        String.format(MESSAGE_METADATA_INCLUDING_PAYLOAD_URL, messageContentType,
                                      payloadType.getPayloadSize(), payloadType.getPayloadUrl())));
        return this;
    }

    public MessageAttributesBuilder withMessageMetadataForPayloadInBody(String messageContentType, int payloadLength) {
        messageAttributes.put("message-metadata", new MessageAttributeValue().withDataType("String")
                .withStringValue(String.format(MESSAGE_METADATA_EXCLUDING_PAYLOAD_URL, messageContentType, payloadLength)));
        return this;
    }

    public MessageAttributesBuilder withAckMessage() {
        messageAttributes.put("ack-message", new MessageAttributeValue().withDataType("String").withStringValue(ackMessage));
        return this;
    }

    public MessageAttributesBuilder withCorrelationId(UUID correlationId) {
        messageAttributes.put("correlation-id", new MessageAttributeValue().withDataType("String")
                .withStringValue(correlationId.toString()));
        return this;
    }

    public Map<String, MessageAttributeValue> build() {
        return messageAttributes;
    }
}
