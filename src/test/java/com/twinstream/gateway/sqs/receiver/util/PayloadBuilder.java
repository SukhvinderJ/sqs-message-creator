package com.twinstream.gateway.sqs.receiver.util;

import java.time.Instant;
import java.time.LocalDateTime;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

public class PayloadBuilder {

    public String withSislPayload() {
        return buildSislPayload();
    }

    public String withJsonPayload(int i) {
        return buildJsonPayload("Batch - " + LocalDateTime.now().format(ISO_LOCAL_DATE_TIME) + " - " + i);
    }

    public String withEmptyPayload(int i) {
        return "message: " + i;
    }

    private String buildJsonPayload(String batchName) {
        return String.format("{\n"
                                     + "  \"batchName\": \"%s\",\n"
                                     + "  \"now\": \"%s\"\n"
                                     + "}", batchName, Instant.now());
    }

    private String buildExportApiCompatiblePayload() {
        return "test";
    }

    private String buildSislPayload() {
        throw new RuntimeException("to be implemented");
    }
}
